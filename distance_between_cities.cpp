#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    vector<double> distances;
    double distance, sum = 0;
    while (cin >> distance) {
        distances.push_back(distance);
        sum += distance;
    }
    sort(distances.begin(), distances.end());
    cout << "Общее расстнояние: " << sum << endl
         << "Наименьшее расстояние: " << distances[0] << endl
         << "Наибольшее рвсстояние: " << distances[distances.size()-1] << endl
         << "Среднее расстояние: " << sum / distances.size();
}