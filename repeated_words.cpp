#include <iostream>
using namespace std;

int main(){
    int number_of_words = 0;
    string prev = " ";
    string curr;
    while (cin >> curr) {
        ++number_of_words;
        if (curr == prev) {
            cout << "Repeated word: " << curr
            << " after " << number_of_words
            << " words" << endl;
        }
        prev = curr;
    }
}