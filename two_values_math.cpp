#include <iostream>
using namespace std;

int main() {
    cout << "Пожалуйста введите два вещественных числа: ";
    double val1 = 0, val2 = 0;
    cin >> val1 >> val2;
    if (val1 > val2) {
        cout << "Большее значение: " << val1 << ", меньшее значение: " << val2 << endl;
    } else {
        cout << "Большее значение: " << val2 << ", меньшее значение: " << val1 << endl;
    }
    cout << "Сумма чисел " << val1 << " и " << val2 << " равна: " << val1 + val2 << endl;
    cout << "Раззность чисел " << val1 << " и " << val2 << " равна: " << val1 - val2 << endl;
    cout << "Произведение чисел " << val1 << " и " << val2 << " равно: " << val1 * val2 << endl;
    cout << "Частное от деления числа " << val1 << " на число " << val2 << " равно: " << val1 / val2 << endl;
}