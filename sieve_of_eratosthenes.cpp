#include <iostream>
#include <vector>
using namespace std;

void init_vector(vector<int>& digits) {
    int start_val = 2;
    for (int& d: digits)
        d = start_val++;
}

void cross_out(int digit, vector<int>& digits) {
    for (int i = digit*digit-2; i < digits.size(); i += digit) {
        digits[i] = 0;
    }
}

void fill_primes(vector<int>& digits, vector<int>& primes) {
    for (int i = 0; i < digits.size(); ++i)
        if (digits[i] != 0)
            primes.push_back(digits[i]);
}

int main() {
    int n = 0;
    cout << "Введите число, до которого нужно посчитать все простые числа: ";
    cin >> n;
    vector<int> digits(n - 1);
    vector<int> primes;
    init_vector(digits);
    for (const int& d: digits) {
        // stop, all other numbers are crossed out
        if (d*d > n)
            break;
        cross_out(d, digits);
    }
    fill_primes(digits, primes);
    for (int& d: primes)
        cout << d << " ";
    cout << endl;
}