#include <iostream>
#include <vector>
using namespace std;

int main() {
    string name;
    int score;
    vector<string> names;
    vector<int> scores;
    bool dupl = false;
    cout << "Введите имена и их баллы:" << endl;
    while (cin >> name >> score) {
        if (name == "NoName")
            break;
        names.push_back(name);
        scores.push_back(score); 
    }
    for (int i = 0; i < names.size(); ++i)
        for (int j = i + 1; j < names.size(); ++j)
            if (names[i] == names[j]) {
                dupl = true;
                break;
            }
    if (dupl) {
        cout << "Ошибка! В наборе есть повторяющиеся имена.";
    } else {
        for (int i = 0; i < names.size(); i++)
            cout << names[i] << " - " << scores[i] << endl;
    }
}