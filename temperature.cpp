#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    vector<double> temps;
    double med;
    for(double temp; cin >> temp;) {
        temps.push_back(temp);
    }
    // Вычисление средней температуры
    double sum = 0;
    for (double t: temps) sum += t;
    cout << "Средняя температура: "
         << sum/temps.size() << endl;
    // Вычисление медианы
    sort(temps.begin(), temps.end());
    if (temps.size() % 2 == 0)
        med = (temps[temps.size()/2 - 1] + temps[temps.size()/2]) / 2;
    else
        med = temps[temps.size()/2];
    cout << "Медианная температура: "
         << med << endl; 
}