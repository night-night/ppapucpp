#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int get_day(string& day) {
    // MonDAy -> monday
    transform(day.begin(), day.end(), day.begin(), ::tolower);
    // все значения для слов не являющиеся валидными
    // запишем в "мусорку" 
    int day_num = 7;
    if (day == "monday" || day == "mon") {
        day_num = 0;
    } else if (day == "tuesday" || day == "tue") {
        day_num = 1;
    } else if (day == "wednesday" || day == "wed") {
        day_num = 2;
    } else if (day == "thursday" || day == "thu") {
        day_num = 3;
    } else if (day == "friday" || day == "fri") {
        day_num = 4;
    } else if (day == "saturday" || day == "sat") {
        day_num = 5;
    } else if (day == "sunday" || day == "sun") {
        day_num = 6;
    }
    return day_num;
}

int main() {
    string day;
    int num;
    vector<int> days(8, 0);
    cout << "Пожалуйста ввводите день (для выхода - \"exit\") и любое число.\n";
    while (cin >> day >> num) {
        if (day == "exit") {
            break;
        } else {
            days[get_day(day)] += num;
        }
    }
    for (int i = 0; i < days.size()-1; ++i) {
        cout << i << ": " << days[i] << endl;
    }
}