#include <iostream>
using namespace std;

int main() {
    cout << "Введите операцию и два вещественных числа: ";
    string operation;
    double val1 = 0, val2 = 0, res_val = 0;
    string res = "Результат выполнения ";
    cin >> operation >> val1 >> val2;
    if (operation == "+" || operation == "plus") {
        res_val = val1 + val2;
    } else if (operation == "-" || operation == "minus") {
        res_val = val1 - val2;
    } else if (operation == "*" || operation == "mul") {
        res_val = val1 * val2;
    } else if (operation == "/" || operation == "dvi") {
        res_val = val1 / val2;
    }     
    cout << res << val1 << " " << operation << " " 
         << val2 << " = " << res_val << endl;
}