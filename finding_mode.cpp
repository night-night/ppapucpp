#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    vector<int> nums;
    int count = 0, mode;
    for (int i; cin >> i; )
        nums.push_back(i);
    sort(nums.begin(), nums.end()); 
    for (int i = 0; i < nums.size()-1; ++i) {
        int temp_count = 0;
        while (nums[i] == nums[i+1]) {
            ++temp_count;
            ++i;
        }
        if (temp_count > count){
            count = temp_count;
            mode = nums[i];
        }
    }
    cout << "Мода: " << mode << " Количество повторений: " << count;
}