#include <iostream>
using namespace std;

int main() {
    int sum_grain = 1, cur_grain = 1, cell = 1;
    while (sum_grain < 2000000000) {
        cout << "Клетка " << cell << endl
             << "Количество зерен в данной клетке " << cur_grain << endl
             << "Суммарное количество зерен " << sum_grain << endl;
        ++cell;
        cur_grain *= 2;
        sum_grain += cur_grain;
    }
}