#include <iostream>
using namespace std;

int main() {
    double l_val, r_val;
    char oper;
    cout << "Пожалуйста введите 2 вещественных числа и символ операции:\n";
    cin >> l_val >> r_val >> oper;
    switch (oper) {
    case '+':
        cout << "Сумма " << l_val << " и " << r_val << " равна " << l_val + r_val << endl;
        break;
    case '-':
        cout << "Разность " << l_val << " и " << r_val << " равна " << l_val - r_val << endl;
        break;
    case '*':
        cout << "Произведение " << l_val << " и " << r_val << " равно " << l_val * r_val << endl;
        break;
    case '/':
        cout << "Частное " << l_val << " и " << r_val << " равно " << l_val / r_val << endl;
        break;
    default:
        cout << "Поддерживаемые символы операции:: +, -, *, /\n";
    }
}