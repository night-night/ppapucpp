#include <iostream>
using namespace std;

int main() {
    cout << "Эта программа переводит мили в километры.\n"
         << "Введите количество миль, которые необходимо перевести в километры: ";
    double miles = 0.0;
    cin >> miles;
    cout << miles << " миль в километрах это: " << miles * 1.609 << endl;
}