#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

constexpr double cm_m = 0.01;   // 1cm = 0.01m
constexpr double in_m = 0.0254; // 1in = 0.0254m
constexpr double ft_m = 0.3048; // 1ft = 0.3048m


double unit_to_meter(double val, string unit) {
    if (unit == "cm")
        val *= cm_m;
    else if (unit == "in")
        val *= in_m;
    else if (unit == "ft")
        val *= ft_m;
    return val;
}

int main() {
    double val, sum = 0; // sum in m
    vector<double> values;
    string unit = ""; // cm, in, ft, m
    while (cin >> val && cin >> unit) {
        if (unit == "cm" || unit == "in" || unit == "ft" || unit == "m"){
            val = unit_to_meter(val, unit);
            values.push_back(val);
            sum += val;
        }
    }
    sort(values.begin(), values.end());
    cout << "Наименьшее введенное значение: " << values[0] << endl
         << "Наибольшее введенное значение: " << values[values.size()-1] << endl
         << "Количество введенных значений: " << values.size() << endl
         << "Сумма введенных значений: " << sum << endl;

    cout << "Введенные значения в метрах: \n";
        for (double val: values)
            cout << val << " ";
}