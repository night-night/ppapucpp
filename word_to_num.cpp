#include <iostream>
using namespace std;

int main() {
    cout << "Пожалуйста, введите число словом: ";
    string num = "";
    string res = "Вы ввели число";
    cin >> num;
    if (num == "ноль") {
        res += " 0";
    } else if (num == "один" ) {
        res += " 1";
    } else if (num == "два") {
        res += " 2";
    } else if (num == "три") {
        res += " 3";
    } else if (num == "четыре") {
        res += " 4";
    } else {
        res += ", которое я не знаю :(";
    }
    cout << res;
}