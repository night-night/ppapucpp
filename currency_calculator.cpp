#include <iostream>
using namespace std;

int main() {
    constexpr double uah_usd = 28.04;
    constexpr double rub_usd = 66.11;
    constexpr double cny_usd = 6.92; 
    constexpr double usd_eur = 1.14; 
    constexpr double usd_gbp = 1.31;
    cout << "Пожалуйста введите количество и вид валюты.\n"
         << "(Обозначения валют: гривна - U, рубль - R, юань - C, евро - E, фунт стерлинга - G)\n"
         << "> ";
    int count = 0;
    char symb = ' ';
    cin >> count >> symb;
    switch (symb) {
    case 'U':
        cout << count << " гривен это " << count / uah_usd << " долларов.\n";
        break;
    case 'R':
        cout << count << " рублей это " << count / rub_usd << " долларов.\n";
        break;
    case 'C':
        cout << count << " юаней это " << count / cny_usd << " долларов.\n";
        break;
    case 'E':
        cout << count << " евро это " << count * usd_eur << " долларов.\n";
        break;
    case 'G':
        cout << count << " фунтов стерлинга это " << count * usd_gbp << " долларов.\n";
        break;
    default:
        cout << "Простите, но я не знаю такой валюты: " << symb;
        break;
    }
}
