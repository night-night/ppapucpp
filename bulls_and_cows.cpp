#include <iostream>
#include <vector>
#include <random>
using namespace std;

int get_bulls(vector<int>& comp, vector<int>& user) {
    int bulls = 0;
    for (int i = 0; i < comp.size(); ++i)
        if (comp[i] == user[i])
            ++bulls;
    return bulls;
}

int get_cows(vector<int>& comp, vector<int>& user) {
    int cows = 0;
    for (int c: comp)
        for (int u: user)
            if (c == u) {
                ++cows;
                break;
            }
    return cows;
}

void fill_comp(vector<int>& comp) {
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dis(0, 9);
    for (int i = 0; i < comp.size(); ++i)
        comp[i] = dis(gen);
}

int main() {
    vector<int> comp(4, -1);
    vector<int> user(4, -1);
    cout << "Давайте поиграем в игру Быки и Коровы\n";
    while(true) {
        fill_comp(comp);
        cout << "Введите 4 разных числа в диапазоне 0-9\n";
        for (int i = 0; i < 4; ++i)
            cin >> user[i]; 
        int bulls = get_bulls(comp, user);  
        int cows = get_cows(comp, user);
        cout << "У Вас " << bulls << " быка и " << cows - bulls << " коровы.\n";
    }
    cout << "Поздравляю! Вы угадали число :)\n";
}