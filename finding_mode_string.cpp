#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    vector<string> words;
    string mode;
    int count = 0;
    // for end press cmd + d
    for (string w; cin >> w;)
        words.push_back(w);
    sort(words.begin(), words.end());
    for (int i = 0; i < words.size()-1; i++) {
        int temp_count = 0;
        while (words[i] == words[i+1]) {
            ++temp_count;
            ++i;
        }
        if (temp_count > count){
            count = temp_count;
            mode = words[i];
        }
    }
    cout << "Наименьшее значение: " << words[0] << endl;
    cout << "Наибольшее значение: " << words[words.size()-1] << endl;
    cout << "Мода: " << mode << " Количество повторений: " << count << endl;
}