#include <iostream>
using namespace std;

string get_tail(int n) {
    if (n == 1) {
        return "ая монета.\n";
    }
    return "ых монет.\n";
}

int main() {
    int cent_1 = 0, cent_5 = 0, cent_10 = 0, cent_25 = 0, cent_50 = 0;
    cout << "Программа для подчета суммарного количества денег.\n";
    cout << "Сколько у Вас одноцентовых монет? ";
    cin >> cent_1;
    cout << "Сколько у Вас пятицентовых монет? ";
    cin >> cent_5;
    cout << "Сколько у Вас десятицентовых монет? ";
    cin >> cent_10;
    cout << "Сколько у Вас двадцатипятицентовых монет? ";
    cin >> cent_25;
    cout << "Сколько у Вас пятидесятицентовых монет? ";
    cin >> cent_50;
    cout << "У вас " << cent_1 << " одноцентов" << get_tail(cent_1);
    cout << "У вас " << cent_5 << " пятицентов" << get_tail(cent_5);
    cout << "У вас " << cent_10 << " десятицентов" << get_tail(cent_10);
    cout << "У вас " << cent_25 << " двадцатипятицентов" << get_tail(cent_25);
    cout << "У вас " << cent_50 << " пятидесятицентов" << get_tail(cent_50);
    int sum = cent_1 + cent_5 * 5 + cent_10 * 10 + cent_25 * 25 + cent_50 * 50;
    cout << "Общая стоимость ваших монет равна " << sum / 100 
         << " долларов " << sum % 100 << " центов.\n";
}