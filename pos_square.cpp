#include <iostream>
using namespace std;

int pos_square(int num) {
    if (num < 0)
        throw runtime_error("Функция pos_sqare. Поддерживаются только положительные числа!\n");
    return num*num;
}

int main() {
    int num;
    while (cin >> num) {
        try {
            cout << "Квадрат числа " << num << " - " 
                 << pos_square(num) << endl;
        } catch (runtime_error& e) {
            cerr << "Error: " << e.what();
        }
    }
}