#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;

void get_result(int user_in, int comp_in) {
    //-1 losе, 0 draw, 1 won
    int diff = user_in - comp_in;
    switch (diff) {
    case -1: case 2:
        cout << "Поздравляю! Вы победили!\n";
        break;
    case 0:
        cout << "Ничья!\n";
        break;
    default:
        cout << "К сожалению Вы проиграли :(\n";
        break;
    }
}

int main() {
    vector<string> values {"Камень", "Ножницы", "Бумага"};
    cout << "Давайте поиграем в игру камень-ножницы-бумага?\n"
         << "Вводите 0 - если выбираете камень, 1 - ножницы, 2 - бумага.\n"
         << "Для выхода введите \"exit\"\n";
    int user_in, comp_in;
    while (cin >> user_in) {
        if (user_in < 0 || user_in > 2) {
            cout << "Пожалуйста, введите 0, 1 или 2\n";
        } else {
            comp_in = rand() % 3;
            cout << "Вы выбрали " << values[user_in]
                 << ". Компьютер выбрал " << values[comp_in] << ".\n";
            get_result(user_in, comp_in);
        }
    }
}