#include <iostream>
#include <cmath>
using namespace std;

double get_discr(double a, double b, double c) {
    return b*b - 4*a*c;
}

int main() {
    double a, b, c, discr;
    double x1, x2;
    cout << "Введите коэффициенты a, b и свободный член c: ";
    cin >> a >> b >> c;
    discr = get_discr(a, b, c);
    if (discr > 0) {
        x1 = (-b - sqrt(discr)) / (2.0*a);
        x2 = (-b + sqrt(discr)) / (2.0*a);
    } else if (discr == 0) {
        x1 = x2 = (-b)/2*a;
    } 
    if (discr < 0) {
        cout << "Уравнение не имеет решения.";
    } else {
        cout << "Уравнение иммет 2 корня: " << x1 << " и " << x2;
    }
}