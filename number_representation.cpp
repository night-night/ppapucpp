#include <iostream>
#include <vector>
using namespace std;

int get_num(const vector<string>& v, const string& s) {
    for (int i = 0; i < v.size(); ++i) {
        if (v[i] == s)
            return i;
    }
    return -1;
}

int main() {
    vector<string> nums {"zero", "one", "two", "three", "four", "five",
                         "six", "seven", "eight", "nine"};
    cout << "Введите число (1, 2, ..) или строку (\"one\", \"two\", ..):\n";
    for (string num; cin >> num;) {
        int n = get_num(nums, num);
        if (n == -1) {
            cout << nums[stoi(num)] << endl;
        } else {
            cout << n << endl;
        }
    }
}