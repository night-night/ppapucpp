#include <iostream>
using namespace std;

int fib(int n) {
    if (n < 2) return n;
    return fib(n-1) + fib(n-2);
}

int fib2(int n) {
    int x = 1;
    int y = 0;
    for (int i = 1; i <= n; i++) {
        x += y;
        y = x - y;
    }
    return y;
}

int main() {
    cout << fib2(2) << endl;
}