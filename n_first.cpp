#include <iostream>
#include <vector>
using namespace std;

int main() {
    int n = 0;
    double num = 0, sum = 0;
    vector<double> v;
    vector<double> diff;
    cout << "Введите количество суммируемых значений: ";
    cin >> n;
    cout << "Введите несколько чисел (| для окончания ввода)\n";
    while (cin >> num)
        v.push_back(num);
    if (v.size() < n)
        throw runtime_error("Переданных чисел меньше, чем требуется просуммировать!");
    for (int i = 0; i < n; ++i)
        sum += v[i];
    cout << "Сумма первых " << n << " чисел (";
    for (int i = 0; i < v.size()-1;++i)
        cout << v[i] << " ";
    cout << v[v.size()-1] << ") равна " << sum << endl;
    for (int i = 0; i < v.size()-1; ++i)
        diff.push_back(v[i] - v[i+1]);
    cout << "Разность соседних чисел: ";
    for (double d: diff)
        cout << d << " ";
}