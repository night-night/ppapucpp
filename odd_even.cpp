#include <iostream>
using namespace std;

int main() {
    cout << "Пожалуйста, введите целое число: ";
    int num = 0;
    cin >> num;
    string res = "нечетным";
    if (num % 2 == 0) {
        res = "четным.";
    }
    cout << "Число " << num << " является " << res;
}