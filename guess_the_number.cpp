#include <iostream>
using namespace std;

int get_mid(int l_val, int r_val) {
    if ((l_val + r_val)%2 == 0)
        return (l_val + r_val) / 2;
    else
        return (l_val + r_val + 1) / 2;
}

int main() {
    cout << "Загадайте число от 1 до 100.\n";
    int l_val = 1, r_val = 100;
    int mid = get_mid(l_val, r_val);
    string ans;
    while (l_val < r_val) {
        cout << "Загаданное число меньше " << mid << "?\n";
        cin >> ans;
        if (ans == "yes") r_val = --mid;
        else if (ans == "no") l_val = mid;
        else cout << "Введите \"yes\" или \"no\"\n";
        mid = get_mid(l_val, r_val);
    }
    cout << "Вы загадали " << l_val;
}