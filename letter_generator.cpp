#include <iostream>
using namespace std;

int main() {
    string first_name, friend_name;
    char friend_sex = 0;
    int age = -1;
    cout << "Введите имя адресата: ";
    cin >> first_name;
    cout << "Дорогой " << first_name << "," << endl;
    cout << "Как дела? У меня все хорошо. Я скучаю по тебе." << endl;
    cout << "Введите имя еще одного друга: ";
    cin >> friend_name;
    cout << "Давно ли ты встречал " << friend_name << "?" << endl;
    cout << "Введите - 'm' если ваш друг мужчина или 'f', если женщина: ";
    cin >> friend_sex;
    cout << "Если ты увидишь " << friend_name << ", пожалуйста, попроси ";
    if (friend_sex =='m') {
        cout << "его";
    } else {
        cout << "ее";
    }
    cout << " позвонить мне." << endl;
    cout << "Введите возраст адресата: ";
    cin >> age;
    if (age <= 0 || age >= 110) {
        throw "Ты шутишь!";
    } else {
        cout << "Я слышал, что ты только что отметил день рождения и тебе исполнилось "
             << age << " лет." << endl;
    }
    if (age < 12) {
        cout << "На следующий год тебе исполнится " << age+1 << " лет." << endl;
    } else if (age == 17) {
        cout << "В следующем году ты сможешь голосовать." << endl;
    } else if (age > 70) {
        cout << "Я надеюсь, что ты не скучаешь на пенсии." << endl;
    }
    cout << "Искренне твой\n\nAydar";
}